
timer.Simple(4, function()

	DarkRP.createEntity("Blue Printer", {
		ent = "boost_printer",
		model = "models/props_c17/consolebox01a.mdl",
		price = 2000,
		max = 2,
		cmd = "buyblueprinter"
	})

	DarkRP.createEntity("Red Printer", {
		ent = "boost_printer_red",
		model = "models/props_c17/consolebox01a.mdl",
		price = 4000,
		max = 2,
		cmd = "buyredprinter"
	})

	DarkRP.createEntity("Green Printer", {
		ent = "boost_printer_green",
		model = "models/props_c17/consolebox01a.mdl",
		price = 8000,
		max = 2,
		cmd = "buygreenprinter"
	})
	
	DarkRP.createEntity("Yellow Printer", {
		ent = "boost_printer_yellow",
		model = "models/props_c17/consolebox01a.mdl",
		price = 16000,
		max = 2,
		cmd = "buyyellowprinter",
	})

	DarkRP.createEntity("Purple Printer", {
		ent = "boost_printer_purple",
		model = "models/props_c17/consolebox01a.mdl",
		price = 32000,
		max = 2,
		cmd = "buypurpleprinter",
	})
	DarkRP.createEntity("Cooling Cell", {
		ent = "boost_cooling",
		model = "models/Items/battery.mdl",
		price = 350,
		max = 1,
		cmd = "buycoolingcell"
	})

	DarkRP.createEntity("Battery", {
		ent = "boost_battery",
		model = "models/Items/car_battery01.mdl",
		price = 250,
		max = 1,
		cmd = "buybattery"
	})

end)
