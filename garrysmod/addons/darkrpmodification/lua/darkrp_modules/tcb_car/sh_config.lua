/*---------------------------------------------------------------------------
	
	Creator: TheCodingBeast - TheCodingBeast.com
	This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. 
	To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
	
---------------------------------------------------------------------------*/

--[[---------------------------------------------------------
	Variables
-----------------------------------------------------------]]
TCBDealer = TCBDealer or {}

TCBDealer.settings = {}
TCBDealer.dealerSpawns = {}
TCBDealer.vehicleTable = {}

--[[---------------------------------------------------------
	Version
-----------------------------------------------------------]]
TCBDealer.version = 1.4

--[[---------------------------------------------------------
	Settings
-----------------------------------------------------------]]
TCBDealer.settings.testDriveLength = 20
TCBDealer.settings.salePercentage = 75
TCBDealer.settings.storeDistance = 400
TCBDealer.settings.colorPicker = true
TCBDealer.settings.randomColor = true
TCBDealer.settings.checkSpawn = false
TCBDealer.settings.autoEnter = false
TCBDealer.settings.precache = true
TCBDealer.settings.debug = false

TCBDealer.settings.frameTitle = "TCB"

--[[---------------------------------------------------------
	Dealer Spawns
-----------------------------------------------------------]]
TCBDealer.dealerSpawns["rp_downtown_tits_v1"] = {
	{
		pos = Vector(-1071.031250, -1312.394287, -131.968750),
		ang = Angle(2.457829, -179.639191, 0.000000),
		mdl = "models/mossman.mdl",

		spawns = {
			{
				pos = Vector(-1254.009277, -1338.506104, -139.968750),
				ang = Angle(3.622066, 88.256462, 0.000000)
			},
			{
				pos = Vector(163, -960, -139),
				ang = Angle(0, 180, 0)
			}
		}
	}
}

--[[---------------------------------------------------------
	Vehicles - http://facepunch.com/showthread.php?t=1481400 / https://www.youtube.com/watch?v=WSTBFk6nX6k
-----------------------------------------------------------]]
TCBDealer.vehicleTable["bently_pmcontinental"] = {
    name = "bently pmcontinental",
    mdl = "models/lonewolfie/bently_pmcontinental.mdl",
	price = 500000,
}

TCBDealer.vehicleTable["mer_g65"] = {
    name = "mercedez g65",
    mdl = "models/lonewolfie/mer_g65.mdl",
	price = 500000,
}
